import React, { useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Button, Input, Icon } from 'react-native-elements';
import { View, Text } from 'react-native';
import { AuthContext } from '../context/AuthContext';
import MainStyles from '../styles/MainStyles';

const StartTest = () => {

  const { setIsLoggedIn } = React.useContext(AuthContext);
  
  return(
    <SafeAreaView style={ MainStyles.safeAreaView }>
      <View style={ MainStyles.fullView }>
        <Text>START TEST SCRREN</Text>
      </View>
    </SafeAreaView>
  );
}

export default StartTest;