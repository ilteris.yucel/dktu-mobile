import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AddPatients from './AddPatient';
import Home from './Home';
import StartTest from './StartTest';
import SignIn from './SignIn';
import SignUp from './SignUp';
import { AuthContext } from '../context/AuthContext';

const Tab = createBottomTabNavigator();

const Root = () => {
  const { isLoggedIn, setIsLoggedIn } = useContext(AuthContext);
  return(
    <NavigationContainer>
      <Tab.Navigator
          backBehavior={'history'}
          screenOptions={({route}) => ({
            tabBarIcon: ({color, size}) => {
            let iconName;
            if (route.name === 'HOME') {
              iconName = 'home';
            } else if (route.name === 'ADD PATIENT') {
              iconName = 'add-circle';
            } else if (route.name === 'START TEST') {
              iconName = 'play-circle';
            }else if (route.name === 'SIGN IN'){
              iconName = 'log-in';
            }else if(route.name === 'SIGN UP'){
              iconName = 'create';
            }else{
              iconName = 'create';
            }
              return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: '#E6D72A',
            tabBarInactiveTintColor: '#F18D9E',
          })}

        >
        {
          isLoggedIn ? (
            <>
              <Tab.Screen name='HOME' component={Home} />
              <Tab.Screen name='ADD PATIENT' component={AddPatients} />
              <Tab.Screen name='START TEST' component={StartTest} />
            </>
          ) : (
            <>
              <Tab.Screen name='SIGN IN' component={SignIn}/>
              <Tab.Screen name='SIGN UP' component={SignUp}/>
            </>
          )
        }
        </Tab.Navigator>

    </NavigationContainer>
  )

}

export default Root;
