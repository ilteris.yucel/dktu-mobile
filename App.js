/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import { SafeAreaProvider } from 'react-native-safe-area-context';
 import Root from './components/Root';
 import AuthProvider from './context/AuthContext';
 


const App  = () => {

  return (
    <SafeAreaProvider>
      <AuthProvider>
        <Root/>
      </AuthProvider>
    </SafeAreaProvider>
  );
};


export default App;
