import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  fullView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  safeAreaView: {
    flex: 1,
  },
});

export default styles;
